package com.example.calculator;

import java.util.List;
import java.util.Stack;

public class ExprManager {
    public String evaluate(List<ExprWithPrecedenceModel> postfixExpr) {
        Stack<String> output = new Stack<>();

        System.out.println("Wes emboh");
        for(ExprWithPrecedenceModel element : postfixExpr) {
            System.out.print(element.getValue());
        }
        int i = 0;
        for(ExprWithPrecedenceModel element : postfixExpr) {
            int precedence = element.getPrecedence();
            if(precedence == 0) {
                output.push(element.getValue());
            } else {
                float operand2 = Float.valueOf(output.pop());
                float operand1 = Float.valueOf(output.pop());
                String operator = element.getValue();

                if(operator.equals("+")) {
                    output.push(Float.toString((operand1 + operand2)));
                } else if(operator.equals("-")) {
                    output.push(Float.toString((operand1 - operand2)));
                } else if(operator.equals("x")) {
                    output.push(Float.toString((operand1 * operand2)));
                } else if(operator.equals("÷")) {
                    output.push(Float.toString((operand1 / operand2)));
                }
            }
            i++;
        }

        String result = output.pop();

        return result;
    }
}
