package com.example.calculator;

public class ExprModel {
    private String value;
    private ValueType valueType;

    public ExprModel(String value, ValueType valueType) {
        this.value = value;
        this.valueType = valueType;
    }

    public String getValue() {
        return this.value;
    }

    public ValueType getValueType() {
        return this.valueType;
    }
}
