package com.example.calculator;

import java.util.ArrayList;
import java.util.List;

public class ExprModelListToExprWithPrecedenceModelList {
    public static List<ExprWithPrecedenceModel> convert(List<ExprModel> expr) {
        List<ExprWithPrecedenceModel> exprPrec = new ArrayList<>();

        // Convert
        for(int i = 0; i < expr.size(); i++) {
            String elementValue = expr.get(i).getValue();
            int precedence = 0;
            if(expr.get(i).getValueType() == ValueType.NUMBER || expr.get(i).getValueType() == ValueType.COMMA) {
                precedence = 0;
            } else {
                if(elementValue.equals("+") || elementValue.equals("-")) {
                    precedence = 2;
                } else if(elementValue.equals("x") || elementValue.equals("÷")) {
                    precedence = 3;
                } else if(elementValue.equals("(") || elementValue.equals(")")) {
                    precedence = 4;
                }
            }

            exprPrec.add(new ExprWithPrecedenceModel(elementValue, precedence));
        }

        // Format
        List<ExprWithPrecedenceModel> exprPrecFormatted = new ArrayList<>();

        int beginIndex = -1;
        int endIndex = -1;

        for(int i = 0; i < exprPrec.size(); i++) {
            int precedence = exprPrec.get(i).getPrecedence();
            if(beginIndex == -1 && precedence == 0) {
                beginIndex = i;
            } else if(beginIndex != -1 && precedence != 0) {
                endIndex = i;

                String temp = "";
                for(int j = beginIndex; j < endIndex; j++) {
                    temp += exprPrec.get(j).getValue();
                }

                exprPrecFormatted.add(new ExprWithPrecedenceModel(temp, 0));
                exprPrecFormatted.add(new ExprWithPrecedenceModel(exprPrec.get(i).getValue(), precedence));

                beginIndex = i + 1;
                endIndex = -1;
            } else if(i == exprPrec.size() - 1) {
                endIndex = exprPrec.size();

                String temp = "";
                for(int j = beginIndex; j < endIndex; j++) {
                    temp += exprPrec.get(j).getValue();
                }

                exprPrecFormatted.add(new ExprWithPrecedenceModel(temp, 0));
            }
        }

        return exprPrecFormatted;
    }
}
