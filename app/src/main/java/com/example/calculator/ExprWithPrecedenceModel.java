package com.example.calculator;

public class ExprWithPrecedenceModel {
    final private String value;
    final private int precedence;

    public ExprWithPrecedenceModel(String value, int precedence) {
        this.value = value;
        this.precedence = precedence;
    }

    public String getValue() {
        return this.value;
    }

    public int getPrecedence() {
        return this.precedence;
    }
}
