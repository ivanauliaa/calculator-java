package com.example.calculator;

public class FloatFormatter {
    public static String convert(String expr) {
        int endIndex = -1;
        int j = 0;

        for(int i = expr.length() - 1; i >= 0; i--) {
            if(expr.charAt(i) != '0') {
                if(expr.charAt(i) == '.') {
                    endIndex = j + 1;
                }  else {
                    endIndex = j;
                }
                break;
            }
            j++;
        }

        String result = expr.substring(0, expr.length() - endIndex);

        return result;
    }
}
