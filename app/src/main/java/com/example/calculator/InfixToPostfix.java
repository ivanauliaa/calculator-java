package com.example.calculator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class InfixToPostfix {
    public static List<ExprWithPrecedenceModel> convert(List<ExprWithPrecedenceModel> expr) {
        Stack<ExprWithPrecedenceModel> output = new Stack<>();
        Stack<ExprWithPrecedenceModel> operator = new Stack<>();
        operator.add(new ExprWithPrecedenceModel("#", -1));

        ExprWithPrecedenceModel topStackOperator = null;
        ExprWithPrecedenceModel selectedOperator = null;

        for (int i = 0; i < expr.size(); i++) {
            if(expr.get(i).getPrecedence() == 0) {
                System.out.println("Value: " + expr.get(i).getValue());
            }
            if (!output.empty()) {
                for (ExprWithPrecedenceModel element : output) {
                    System.out.print(element.getValue());
                }
                System.out.println("\nSize: " + output.size());
                System.out.println("===");
            }
            int precedence = expr.get(i).getPrecedence();

            if (precedence == 0) {
                output.push(expr.get(i));
            } else if (expr.get(i).getValue().equals("(")) {
                operator.push(expr.get(i));
            } else if (expr.get(i).getValue().equals(")")) {
                topStackOperator = operator.lastElement();
                while (!topStackOperator.getValue().equals("(")) {
                    ExprWithPrecedenceModel temp = operator.pop();
                    output.push(temp);
                    topStackOperator = operator.lastElement();
                    System.out.println("oprsize: " + operator.size());
                }
                operator.pop();
            } else {
                selectedOperator = expr.get(i);
                topStackOperator = operator.lastElement();
                System.out.println(selectedOperator.getValue() + " < " + topStackOperator.getValue());
                if (selectedOperator.getPrecedence() >= topStackOperator.getPrecedence()) {
                    operator.push(selectedOperator);
                } else {
                    if (topStackOperator.getValue().equals(("("))) {
                        operator.push(selectedOperator);
                    } else {
                        while(selectedOperator.getPrecedence() < topStackOperator.getPrecedence()) {
                            if(topStackOperator.getValue().equals("(")) {
//                                System.out.println("anomali: " + topStackOperator.getValue());
//                                operator.push(selectedOperator);
                                break;
                            } else {
                                ExprWithPrecedenceModel temp = operator.pop();
                                output.push(temp);
                            }
                            topStackOperator = operator.lastElement();
                        }
                        operator.push(selectedOperator);
                    }
//                        while (topStackOperator.getPrecedence() > selectedOperator.getPrecedence()) {
//                            if (topStackOperator.getValue().equals("(")) {
//                                operator.push(selectedOperator);
//                                break;
//                            } else if (topStackOperator.getPrecedence() < selectedOperator.getPrecedence()) {
//                                break;
//                            } else {
//                                ExprWithPrecedenceModel temp = operator.pop();
//                                output.push(temp);
//                            }
//                            if(!operator.empty()) {
//                                topStackOperator = operator.lastElement();
//                            } else {
//                                break;
//                            }
//                        }
                }
            }
        }

        System.out.println("oprsize: " + operator.size());
        System.out.println("outsize: " + output.size());
        System.out.println("===");
        // pop semua operator yg tersisa dan push ke output
        while (!operator.lastElement().getValue().equals("#")) {
            ExprWithPrecedenceModel temp = operator.pop();
            System.out.println("Value: " + temp.getValue());
            output.push(temp);
            for (ExprWithPrecedenceModel element1 : output) {
                System.out.print(element1.getValue());
            }
            System.out.println("\nSize: " + output.size());
        }

        List<ExprWithPrecedenceModel> result = new ArrayList<>(output);
        System.out.println("Print list buat dikembaliin");
        for (ExprWithPrecedenceModel element1 : result) {
            System.out.print(element1.getValue());
        }

        System.out.println("Result size: " + result.size());
//        System.out.println("mau masuk perulangan convert data type slur");
//        System.out.println("\nSize: " + output.size());
//        for(int j = 0; j < 8; j++) {
//            result.add(output.get(j));
//            System.out.print(result.get(j).getValue());
//        }

        return result;
    }
}
