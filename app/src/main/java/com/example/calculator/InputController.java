package com.example.calculator;

import java.util.ArrayList;
import java.util.List;

public class InputController {
    private InputState inputState;
    private CommaState commaState;
    private ParenthesesState parenthesesState;
    private InputModel model;

    public InputController() {
        this.inputState = InputState.EMPTY;
        this.commaState = CommaState.NOT_EXIST;
        this.parenthesesState = ParenthesesState.CLOSE;
        this.model = new InputModel();
    }

    public String value() {
        return this.model.getStringValue();
    }

    public void input(CharSequence buttonValue) {
        String value = buttonValue.toString();
        String collection[][] = { // number, operator, enter, clear, delete, brace, comma
                {
                        "0", "1", "2", "3", "4",
                        "5", "6", "7", "8", "9"
                },
                {
                        "÷", "x", "-", "+"
                },
                {
                        "="
                },
                {
                        "AC"
                },
                {
                        "DEL"
                },
                {
                        "( )"
                },
                {
                        "%"
                },
                {
                        "."
                }
        };

        boolean isFound = false; // untuk mencari method sesuai jenis value
        for (int i = 0; i < collection.length; i++) {
            for (int j = 0; j < collection[i].length; j++) {
                if (collection[i][j] == null) {
                    continue;
                } else {
                    if (value.equals(collection[i][j])) {
                        if (i == 0) {
                            this.number(value);
                        } else if (i == 1) {
                            this.operator(value);
                        } else if (i == 2) {
                            this.enter();
                        } else if (i == 3) {
                            this.reset();
                        } else if (i == 4) {
                            this.delete();
                        } else if (i == 5) {
                            this.parentheses();
                        } else if (i == 6) {
                            this.percentage(value);
                        } else {
                            this.comma(value);
                        }
                        isFound = true;
                        break;
                    }
                }
            }
            if (isFound) {
                break;
            }
        }
    }

    private void percentage(String value) {
        List<ExprModel> recentValue = this.model.getAllValue();
        ExprModel lastValue = recentValue.get(recentValue.size() - 1);

        if (lastValue.getValueType().equals(ValueType.NUMBER)) {
            int index = -1;
            for (int i = recentValue.size() - 1; i >= 0; i--) {
                if (recentValue.get(i).getValueType().equals(ValueType.OPERATOR)) {
                    index = i + 1;
                    break;
                } else if(i == 0) {
                    index = i;
                }
            }

            if (index != -1) {
                String newValueString = "";
                for (int i = index; i < recentValue.size(); i++) {
                    newValueString += recentValue.get(i).getValue();
                }

                float newValueFloat = Float.valueOf(newValueString) / 100;
                String newValueFloatString = Float.toString(newValueFloat);
                newValueFloatString = FloatFormatter.convert(newValueFloatString);

                List<ExprModel> newValue = new ArrayList<>();
                if (index != 0) {
                    for (int i = 0; i < index; i++) {
                        newValue.add(recentValue.get(i));
                    }
                }

                List<ExprModel> finalResult = StringToExprModelList.convert(newValueFloatString);
                for(ExprModel element : finalResult) {
                    newValue.add(element);
                }
                this.model.setValue(newValue);
            }
        }
    }

    private void operator(String value) {
        List<ExprModel> recentValue = this.model.getAllValue();
        ExprModel lastValue = recentValue.get(recentValue.size() - 1);

        if (lastValue.getValueType() == ValueType.NUMBER) {
            recentValue.add(new ExprModel(value, ValueType.OPERATOR));

            this.model.setValue(recentValue);

            if (this.commaState == CommaState.EXIST) {
                this.commaState = CommaState.NOT_EXIST;
            }
        }

        this.inputState = InputState.FILLED;
    }

    private void enter() {
        if(this.inputState != InputState.EMPTY && this.parenthesesState == ParenthesesState.CLOSE) {
            ExprManager exprManager = new ExprManager();
            List<ExprModel> recentValue = this.model.getAllValue();

            // convert and format untuk memudahkan convert ke postfix
            List<ExprWithPrecedenceModel> expr = ExprModelListToExprWithPrecedenceModelList.convert(recentValue);

            // convert infix to postfix
            List<ExprWithPrecedenceModel> postfixExpr = InfixToPostfix.convert(expr);
            System.out.println("Print terima kembalian");
            for (ExprWithPrecedenceModel element1 : postfixExpr) {
                System.out.print(element1.getValue());
            }

            // evaluate expr postfix
            String result = exprManager.evaluate(postfixExpr);

            // format penulisan float
            result = FloatFormatter.convert(result);

            // convert string ke list of exprmodel biar bisa diset nilainya ke view
            List<ExprModel> finalResult = StringToExprModelList.convert(result);

            // setvalue model
            this.model.setValue(finalResult);

            // set state, jika size 1 dan value = 0 maka state empty, jika tidak maka state filled
            if (finalResult.size() == 1 && finalResult.get(0).getValue().equals("0")) {
                this.inputState = InputState.EMPTY;
            } else {
                this.inputState = InputState.FILLED;
            }
        }
    }

    private void reset() {
        this.model.resetValue();
        this.inputState = InputState.EMPTY;
        this.commaState = CommaState.NOT_EXIST;
        this.parenthesesState = ParenthesesState.CLOSE;
    }

    private void number(String value) {
        // tambah value
        List<ExprModel> recentValue = this.model.getAllValue();

        if (this.inputState == InputState.EMPTY) {
            if (value.equals("0")) {
                return;
            } else {
                recentValue.set(0, new ExprModel(value, ValueType.NUMBER));

                this.model.setValue(recentValue);
            }
        } else {
            recentValue.add(new ExprModel(value, ValueType.NUMBER));

            this.model.setValue(recentValue);
        }
        this.inputState = InputState.FILLED;
    }

    private void delete() {
        List<ExprModel> recentValue = this.model.getAllValue();

        if (recentValue.size() == 1) {
            this.model.resetValue();
            this.inputState = InputState.EMPTY;
        } else {
            ExprModel lastValue = this.model.getValueByIndex(recentValue.size() - 1);
            if (lastValue.getValueType().equals(ValueType.COMMA)) {
                this.commaState = CommaState.NOT_EXIST;
            } else if (lastValue.getValueType().equals(ValueType.OPERATOR)) {
                boolean foundComma = false;

                for (int i = recentValue.size() - 1; i >= 0; i--) {
                    if (recentValue.get(i).getValueType().equals(ValueType.COMMA)) {
                        foundComma = true;
                    } else if (recentValue.get(i).getValueType().equals(ValueType.OPERATOR)) {
                        break;
                    }
                }

                if (foundComma) {
                    this.commaState = CommaState.EXIST;
                }
            }

            this.model.deleteLastValue();
        }
    }

    private void comma(String value) {
        List<ExprModel> recentValue = this.model.getAllValue();
        ExprModel lastValue = this.model.getValueByIndex(recentValue.size() - 1);

        if (this.commaState == CommaState.NOT_EXIST && lastValue.getValueType().equals(ValueType.NUMBER)) {
            recentValue.add(new ExprModel(value, ValueType.COMMA));

            this.commaState = CommaState.EXIST;
            this.model.setValue(recentValue);

            this.inputState = InputState.FILLED;
        }
    }

    private void parentheses() {
        List<ExprModel> recentValue = this.model.getAllValue();
        ExprModel lastValue = this.model.getValueByIndex(recentValue.size() - 1);

        if(this.parenthesesState == ParenthesesState.CLOSE) {
            if(lastValue.getValueType().equals(ValueType.OPERATOR)) {
                recentValue.add(new ExprModel("(", ValueType.PARENTHESES));
                this.parenthesesState = ParenthesesState.OPEN;

                this.model.setValue(recentValue);
            } else if(this.inputState == InputState.EMPTY) {
                recentValue.set(0, new ExprModel("(", ValueType.PARENTHESES));
                this.parenthesesState = ParenthesesState.OPEN;
                this.inputState = InputState.FILLED;

                this.model.setValue(recentValue);
            }
        } else {
            // kasih )
            if(lastValue.getValueType().equals(ValueType.NUMBER) || lastValue.getValueType().equals(ValueType.PARENTHESES)) {
                recentValue.add(new ExprModel(")", ValueType.PARENTHESES));
                this.parenthesesState = ParenthesesState.CLOSE;

                this.model.setValue(recentValue);
            }
        }
    }
}

