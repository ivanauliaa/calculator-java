package com.example.calculator;

import android.renderscript.Sampler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InputModel {
    private List<ExprModel> value = new ArrayList<>();

    public InputModel() {
        this.emptyValue();
    }

    public List<ExprModel> getAllValue() {
        return this.value;
    }

    public ExprModel getValueByIndex(int index) {
        return this.value.get(index);
    }

    public String getStringValue() {
        String stringValue = "";

        for (ExprModel element : this.value) {
            stringValue += element.getValue();
        }

        return stringValue;
    }

    public void emptyValue() {
        List<ExprModel> temp = new ArrayList<>();
        temp.add(new ExprModel("0", ValueType.NUMBER));
        this.value = temp;
    }

    public void setValue(List<ExprModel> value) {
        this.value = value;
    }

    public void resetValue() {
        emptyValue();
    }

    public void deleteLastValue() {
//        this.value = this.value.substring(0, this.value.length() - 1);
        this.value.remove(this.value.size() - 1);
    }
}
