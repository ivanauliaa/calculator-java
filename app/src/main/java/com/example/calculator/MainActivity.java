package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    InputController controller = new InputController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView = findViewById(R.id.input);
        textView.setText(controller.value());

        String buttonText[][] = {
                {"AC", "DEL", "%", "÷",},
                {"7", "8", "9", "x",},
                {"4", "5", "6", "-",},
                {"1", "2", "3", "+",},
                {".", "0", "( )", "=",},
        };

        LinearLayout container = (LinearLayout) findViewById(R.id.button_grid);
        container.setOrientation(LinearLayout.VERTICAL);

        LinearLayout row = new LinearLayout(this);
        row.setOrientation(LinearLayout.VERTICAL);

        for (int i = 0; i < buttonText.length; i++) {
            LinearLayout col = new LinearLayout(this);
            col.setOrientation(LinearLayout.HORIZONTAL);

            for (int j = 0; j < buttonText[i].length; j++) {
                Button button = new Button(this);

                button.setText(buttonText[i][j]);
                button.setTextSize(35);
                button.setBackground(ContextCompat.getDrawable(this, R.drawable.button_bg));
                button.setOnClickListener(v -> updateViewValue(button.getText()));

                col.addView(button);
            }
            row.addView(col);
        }
        container.addView(row);
    }

    void updateViewValue(CharSequence buttonValue) {
        controller.input(buttonValue);

        TextView textView = findViewById(R.id.input);
        textView.setText(controller.value());
    }
}
