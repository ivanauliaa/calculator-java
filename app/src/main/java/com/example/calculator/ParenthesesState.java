package com.example.calculator;

public enum ParenthesesState {
    OPEN,
    CLOSE
}
