package com.example.calculator;

import java.util.ArrayList;
import java.util.List;

public class StringToExprModelList {
    public static List<ExprModel> convert(String expr) {
        List<ExprModel> result = new ArrayList<>();
        String collection[][] = { // number, operator, enter, clear, delete, brace
                {
                        "0", "1", "2", "3", "4",
                        "5", "6", "7", "8", "9"
                },
                {
                        "."
                },
        };

        for(int i = 0; i < expr.length(); i++) {
            String value = Character.toString(expr.charAt(i));
            ValueType valueType = ValueType.NUMBER;

            boolean isFound = false;
            for (int j = 0; j < collection.length; j++) {
                for (int k = 0; k < collection[j].length; k++) {
                    if (collection[j][k] == null) {
                        continue;
                    } else {
                        if (value.equals(collection[j][k])) {
                            if (j == 0) {
                                valueType = ValueType.NUMBER;
                            }  else {
                                valueType = ValueType.COMMA;
                            }
                            isFound = true;
                            break;
                        }
                    }
                }
                if (isFound) {
                    break;
                }
            }
            result.add(new ExprModel(value, valueType));
        }

        return result;
    }
}
