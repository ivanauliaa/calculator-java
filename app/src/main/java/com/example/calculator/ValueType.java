package com.example.calculator;

public enum ValueType {
    NUMBER,
    OPERATOR,
    PARENTHESES,
    COMMA,
}
